#!/bin/bash

sudo apt-get install nfs-common
git clone https://github.com/aws/efs-utils
cd efs-utils
sudo apt-get update
sudo apt-get -y install binutils
sudo ./build-deb.sh
sudo apt-get -y install ./build/amazon-efs-utils*deb

sudo mkdir /opt/efs-system
sudo mount -t efs ${efs_id}:/ /opt/efs-system