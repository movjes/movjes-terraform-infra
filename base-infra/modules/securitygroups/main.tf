locals {
  default_tags = {
    Environment = terraform.workspace
    Name        = "${var.identifier}-${terraform.workspace}"
  }
  tags = merge(local.default_tags, var.tags)
}

resource "aws_security_group" "db-instance" {
  name        = "${var.identifier}-db-${terraform.workspace}"
  description = "Allow TLS inbound traffic to the db instance"
  vpc_id      = var.vpc.id
  tags        = local.tags
}

resource "aws_security_group_rule" "db_ingress_rules" {

  count                    = length(var.db_ingress_rule_list)
  security_group_id        = aws_security_group.db-instance.id
  source_security_group_id = var.db_ingress_rule_list[count.index].source_security_group_id
  cidr_blocks              = var.db_ingress_rule_list[count.index].cidr_blocks
  description              = var.db_ingress_rule_list[count.index].description
  from_port                = var.db_ingress_rule_list[count.index].from_port
  protocol                 = var.db_ingress_rule_list[count.index].protocol
  to_port                  = var.db_ingress_rule_list[count.index].to_port
  type                     = "ingress"
}

resource "aws_security_group_rule" "db_egress_rules" {
  count = length(var.db_egress_rule_list)

  source_security_group_id = var.db_egress_rule_list[count.index].source_security_group_id
  security_group_id        = aws_security_group.db-instance.id
  cidr_blocks              = var.db_egress_rule_list[count.index].cidr_blocks
  description              = var.db_egress_rule_list[count.index].description
  from_port                = var.db_egress_rule_list[count.index].from_port
  protocol                 = var.db_egress_rule_list[count.index].protocol
  to_port                  = var.db_egress_rule_list[count.index].to_port
  type                     = "egress"
}

resource "aws_security_group" "alb-sg" {
  name        = "${var.identifier}-alb-${terraform.workspace}"
  description = "Allow TLS inbound traffic to the application load balancer"
  vpc_id      = var.vpc.id
  tags        = local.tags
}

resource "aws_security_group_rule" "alb_ingress_rules" {

  count                    = length(var.alb_ingress_rule_list)
  security_group_id        = aws_security_group.alb-sg.id
  source_security_group_id = var.alb_ingress_rule_list[count.index].source_security_group_id
  cidr_blocks              = var.alb_ingress_rule_list[count.index].cidr_blocks
  description              = var.alb_ingress_rule_list[count.index].description
  from_port                = var.alb_ingress_rule_list[count.index].from_port
  protocol                 = var.alb_ingress_rule_list[count.index].protocol
  to_port                  = var.alb_ingress_rule_list[count.index].to_port
  type                     = "ingress"
}

resource "aws_security_group_rule" "alb_egress_rules" {
  count = length(var.alb_egress_rule_list)

  source_security_group_id = var.alb_egress_rule_list[count.index].source_security_group_id
  security_group_id        = aws_security_group.alb-sg.id
  cidr_blocks              = var.alb_egress_rule_list[count.index].cidr_blocks
  description              = var.alb_egress_rule_list[count.index].description
  from_port                = var.alb_egress_rule_list[count.index].from_port
  protocol                 = var.alb_egress_rule_list[count.index].protocol
  to_port                  = var.alb_egress_rule_list[count.index].to_port
  type                     = "egress"
}

resource "aws_security_group" "service-asg" {
  name        = "${var.identifier}-service-${terraform.workspace}"
  description = "Allow TLS inbound traffic to the application load balancer"
  vpc_id      = var.vpc.id
  tags        = local.tags
}

resource "aws_security_group_rule" "service_ingress_rules" {

  count                    = length(var.service_ingress_rule_list)
  security_group_id        = aws_security_group.service-asg.id
  source_security_group_id = var.service_ingress_rule_list[count.index].source_security_group_id
  cidr_blocks              = var.service_ingress_rule_list[count.index].cidr_blocks
  description              = var.service_ingress_rule_list[count.index].description
  from_port                = var.service_ingress_rule_list[count.index].from_port
  protocol                 = var.service_ingress_rule_list[count.index].protocol
  to_port                  = var.service_ingress_rule_list[count.index].to_port
  type                     = "ingress"
}

resource "aws_security_group_rule" "service_egress_rules" {
  count = length(var.service_egress_rule_list)

  source_security_group_id = var.service_egress_rule_list[count.index].source_security_group_id
  security_group_id        = aws_security_group.service-asg.id
  cidr_blocks              = var.service_egress_rule_list[count.index].cidr_blocks
  description              = var.service_egress_rule_list[count.index].description
  from_port                = var.service_egress_rule_list[count.index].from_port
  protocol                 = var.service_egress_rule_list[count.index].protocol
  to_port                  = var.service_egress_rule_list[count.index].to_port
  type                     = "egress"
}

resource "aws_security_group" "efs-asg" {
  name        = "${var.identifier}-efs-${terraform.workspace}"
  description = "Allow TLS inbound traffic to the efs"
  vpc_id      = var.vpc.id
  tags        = local.tags
}

resource "aws_security_group_rule" "efs_ingress_rules" {

  count                    = length(var.efs_ingress_rule_list)
  security_group_id        = aws_security_group.efs-asg.id
  source_security_group_id = var.efs_ingress_rule_list[count.index].source_security_group_id
  cidr_blocks              = var.efs_ingress_rule_list[count.index].cidr_blocks
  description              = var.efs_ingress_rule_list[count.index].description
  from_port                = var.efs_ingress_rule_list[count.index].from_port
  protocol                 = var.efs_ingress_rule_list[count.index].protocol
  to_port                  = var.efs_ingress_rule_list[count.index].to_port
  type                     = "ingress"
}

resource "aws_security_group_rule" "efs_egress_rules" {
  count = length(var.efs_egress_rule_list)

  source_security_group_id = var.efs_egress_rule_list[count.index].source_security_group_id
  security_group_id        = aws_security_group.efs-asg.id
  cidr_blocks              = var.efs_egress_rule_list[count.index].cidr_blocks
  description              = var.efs_egress_rule_list[count.index].description
  from_port                = var.efs_egress_rule_list[count.index].from_port
  protocol                 = var.efs_egress_rule_list[count.index].protocol
  to_port                  = var.efs_egress_rule_list[count.index].to_port
  type                     = "egress"
}

resource "aws_security_group" "bastion-sg" {
  name        = "${var.identifier}-bastion-${terraform.workspace}"
  description = "Allow TLS inbound traffic to the efs"
  vpc_id      = var.vpc.id
  tags        = local.tags
}

resource "aws_security_group_rule" "bastion_ingress_rules" {

  count                    = length(var.bastion_ingress_rule_list)
  security_group_id        = aws_security_group.bastion-sg.id
  source_security_group_id = var.bastion_ingress_rule_list[count.index].source_security_group_id
  cidr_blocks              = var.bastion_ingress_rule_list[count.index].cidr_blocks
  description              = var.bastion_ingress_rule_list[count.index].description
  from_port                = var.bastion_ingress_rule_list[count.index].from_port
  protocol                 = var.bastion_ingress_rule_list[count.index].protocol
  to_port                  = var.bastion_ingress_rule_list[count.index].to_port
  type                     = "ingress"
}

resource "aws_security_group_rule" "bastion_egress_rules" {
  count = length(var.bastion_egress_rule_list)

  source_security_group_id = var.bastion_egress_rule_list[count.index].source_security_group_id
  security_group_id        = aws_security_group.bastion-sg.id
  cidr_blocks              = var.bastion_egress_rule_list[count.index].cidr_blocks
  description              = var.bastion_egress_rule_list[count.index].description
  from_port                = var.bastion_egress_rule_list[count.index].from_port
  protocol                 = var.bastion_egress_rule_list[count.index].protocol
  to_port                  = var.bastion_egress_rule_list[count.index].to_port
  type                     = "egress"
}