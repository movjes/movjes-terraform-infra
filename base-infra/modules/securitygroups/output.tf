output "output" {
  value = {
    db_sg          = aws_security_group.db-instance.id
    alb_sg         = aws_security_group.alb-sg.id
    efs_sg         = aws_security_group.efs-asg.id
    service_sg     = aws_security_group.service-asg.id
    ssh_sg         = aws_security_group.bastion-sg.id
  }
}
