locals {
  default_tags = {
    Environment = terraform.workspace
    Name        = "${var.identifier}-${terraform.workspace}"
  }
  tags = merge(local.default_tags, var.tags)
}

resource "aws_ecs_cluster" "cluster" {
  name = "${var.identifier}-${terraform.workspace}"

  tags = local.tags
}