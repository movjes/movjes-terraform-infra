identifier = "movjes"
region     = "us-east-1"
lb_main_certificate_arn = "arn:aws:acm:us-east-1:648600599342:certificate/0a30da05-a0c1-4fb9-91ec-f8edf3a1e63f"
database_name = "movjes"
db_storage = 50
db_instance_class = "db.t2.small"
#vpc
vpc_settings = {
    application_subnets = ["172.20.12.0/22", "172.20.16.0/22","172.20.20.0/22"]
    public_subnets      = ["172.20.24.0/22", "172.20.28.0/22", "172.20.32.0/22"]
    dns_hostnames       = true
    data_subnets        = ["172.20.0.0/22", "172.20.4.0/22","172.20.8.0/22"]
    dns_support         = true
    tenancy             = "default"
    cidr                = "172.20.0.0/16"
}

image_id = "ami-0739f8cdb239fe9ae" # ubuntu 18.04
key_name = "ricardo_key_aws"
volume_size = 50
instance_type = "t2.micro"
min_size = 0
max_size = 0