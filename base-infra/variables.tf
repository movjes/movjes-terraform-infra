variable "region" {}

variable "identifier" {}

variable "lb_main_certificate_arn" {}

variable "database_name" {}

variable "db_storage" {}

variable "db_instance_class" {}

variable "vpc_settings" {
  description = "Map of AWS VPC settings"
  default = {
    application_subnets = ["172.20.16.0/22", "172.20.20.0/22"]
    public_subnets      = ["172.20.0.0/22", "172.20.4.0/22"]
    dns_hostnames       = true
    data_subnets        = ["172.20.8.0/22", "172.20.12.0/22"]
    dns_support         = true
    tenancy             = "default"
    cidr                = "172.20.0.0/16"
  }
  type = object({
    application_subnets = list(string)
    public_subnets      = list(string)
    data_subnets        = list(string)
    dns_hostnames       = bool,
    dns_support         = bool,
    tenancy             = string,
    cidr                = string
  })
}

variable "image_id" {
  description = "AMI id to use by the autoscaling group"
  type        = string
}

variable "key_name" {
  description = "ssh key pair to access the instances"
  type        = string
}

variable "volume_size" {
  description = "size of the root volume for the instance"
  type        = number
}

variable "instance_type" {
  description = "size of the ec2 instance"
  type        = string
}

variable "min_size" {
  description = "Minimum number of instances in the ASG"
  default     = 1
  type        = number
}

variable "max_size" {
  description = "Maximun number of instances in the ASG"
  default     = 1
  type        = number
}