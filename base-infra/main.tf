data "aws_availability_zones" "azs" {}

module "vpc" {
  source            = "./modules/vpc"
  identifier        = var.identifier
  region            = var.region
  vpc_settings      = var.vpc_settings
}


module "security_groups" {
  source              = "./modules/securitygroups"
  identifier          = var.identifier
  vpc                 = module.vpc.output.vpc
}

module "ecs" {
  source                       = "./modules/ecs"
  identifier                   = var.identifier
}

module "alb" {
  source              = "./modules/alb"
  identifier          = var.identifier
  security_groups     = [tostring(module.security_groups.output.alb_sg)]
  subnet_ids          = module.vpc.output.public_subnets.*.id
  vpc_id              = module.vpc.output.vpc_id
  alb_certificate_arn = [var.lb_main_certificate_arn]
}

module "efs" {
  source              = "./modules/efs"
  identifier          = var.identifier
  efs-sgs             = [tostring(module.security_groups.output.efs_sg)]
  subnets             = module.vpc.output.data_subnets.*.id
}

module "rds" {
  source              = "./modules/rds"

  rds_parameter_group_family = "postgres10"
  rds_allocated_storage      = var.db_storage
  rds_master_username        = "master"
  rds_engine_version         = 10.15
  rds_instance_class         = var.db_instance_class
  rds_database_name          = var.database_name
  security_groups            = [tostring(module.security_groups.output.db_sg)]
  identifier                 = var.identifier
  subnets                    = module.vpc.output.data_subnets.*.id
  engine                     = "postgres"
  vpc_id                     = module.vpc.output.vpc_id

  
}

# creating the autoscaling group resource for the ssh bastion
data "template_file" "user_data" {
  template = file("./templates/ec2-user-data.sh")
  vars = {
    efs_id = module.efs.output.efs_id
  }
}

module "asg" {
    source = "./modules/asg"
    associate_public_ip_address = true
    user_data_base64     = base64encode(data.template_file.user_data.rendered)
    security_groups      = [tostring(module.security_groups.output.ssh_sg)]
    identifier           = var.identifier
    image_id             = var.image_id
    key_name             = var.key_name
    volume_size          = var.volume_size
    instance_type        = var.instance_type
    subnets              = module.vpc.output.public_subnets.*.id
    min_size             = var.min_size
    max_size             = var.max_size
    depends_on = [
      module.efs,
    ]
}