# adding useful outputs from the infrastructure
output "output" {
  value = {
    # vpc outputs
    application_subnets     = module.vpc.output.application_subnets.*.id
    public_subnets          = module.vpc.output.public_subnets.*.id
    data_subnets            = module.vpc.output.data_subnets.*.id
    vpc_id                  = module.vpc.output.vpc_id
    
    # ecs outputs
    ecs_cluster_arn         = module.ecs.output.cluster.arn
    ecs_cluster_name        = module.ecs.output.cluster.name

    # security groups
    service_sg              = module.security_groups.output.service_sg

    # efs volume
    efs_volume_id           = module.efs.output.efs_id
  }
}