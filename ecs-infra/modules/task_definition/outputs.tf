output "output" {
  value = {
    task_definition = aws_ecs_task_definition.task
    testing_td = zipmap(local.env_vars_keys, local.env_vars_values)
  }
}
