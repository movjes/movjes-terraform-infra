	locals {
  default_tags = {
    Environment = terraform.workspace
    Name        = "${var.identifier}-${terraform.workspace}"
  }
  tags = merge(local.default_tags, var.tags)
}
# execution role for ecs
data "aws_iam_policy" "ecs-execution-role" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

data "aws_iam_policy" "ecs-execution-cloudwatch" {
  arn = "arn:aws:iam::aws:policy/CloudWatchFullAccess"
}

data "aws_iam_policy_document" "policy-assume" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}
data "aws_iam_policy_document" "secretsmanager-access" {
  statement {
    resources = [
      "*"
    ]
    actions   = [
      "ssm:GetParameters",
      "secretsmanager:GetSecretValue",
      "kms:Decrypt"
    ]
  }
}
resource "aws_iam_policy" "ecs-execution" {
  description = "policy to give permissions to ecs executions"
  policy      = data.aws_iam_policy_document.secretsmanager-access.json
  name        = "${var.identifier}-${terraform.workspace}-ecs-execution"
}
resource "aws_iam_role" "ecs-role-execution" {
  assume_role_policy = data.aws_iam_policy_document.policy-assume.json
  path               = "/"
  name               = "${var.identifier}-${terraform.workspace}-execution-role"
}
resource "aws_iam_role_policy_attachment" "execution-attach" {
  role       = aws_iam_role.ecs-role-execution.name
  policy_arn = aws_iam_policy.ecs-execution.arn
}
resource "aws_iam_role_policy_attachment" "execution-attach-permissions" {
  role       = aws_iam_role.ecs-role-execution.name
  policy_arn = data.aws_iam_policy.ecs-execution-role.arn
}

resource "aws_iam_role_policy_attachment" "execution-attach-cloudwatch" {
  role       = aws_iam_role.ecs-role-execution.name
  policy_arn = data.aws_iam_policy.ecs-execution-cloudwatch.arn
}

# execution role for ecs tasks
data "aws_iam_policy_document" "es-permissions" {
  statement {
    resources = [
      "*"
    ]
    actions   = [
      "es:*",
      "lambda:InvokeAsync",
      "lambda:InvokeFunction"
    ]
  }
}
resource "aws_iam_policy" "task-permissions" {
  description = "policy to give permissions to ecs tasks"
  policy      = data.aws_iam_policy_document.es-permissions.json
  name        = "${var.identifier}-${terraform.workspace}-task-permissions"
}
resource "aws_iam_role" "task-role-permissions" {
  assume_role_policy = data.aws_iam_policy_document.policy-assume.json
  path               = "/"
  name               = "${var.identifier}-${terraform.workspace}-task-role"
}
resource "aws_iam_role_policy_attachment" "task-permissions-attach" {
  role       = aws_iam_role.task-role-permissions.name
  policy_arn = aws_iam_policy.task-permissions.arn
}
resource "aws_ecs_task_definition" "task" {
  requires_compatibilities = ["FARGATE"]
  container_definitions    = "[ ${local.json_map} ]"
  execution_role_arn       = aws_iam_role.ecs-role-execution.arn
  task_role_arn            = aws_iam_role.task-role-permissions.arn
  network_mode             = "awsvpc"
  memory                   = var.container_memory
  family                   = "${var.identifier}-${terraform.workspace}"
  cpu                      = var.container_cpu
  tags = local.tags

  volume {
    name = "static_storage"

    efs_volume_configuration {
      file_system_id          = var.efs_volume_id
      root_directory          = "odoo-static/"
    }
  }

  volume {
    name = "config"

    efs_volume_configuration {
      file_system_id          = var.efs_volume_id
      root_directory          = "odoo-config/"
    }
  }
}
