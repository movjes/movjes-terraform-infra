variable "identifier" {
  description = "The name for the cluster"
  type        = string
}

variable "container_port" {
  description = "The port that the container service runs on"
  type        = number
}

variable "service_asgs" {
  description = "service security groups for the service"
  type        = list(string)
}

variable "vpc_subnets" {
  description = "subnets id where the service will be deployed"
  type        = list(string)
}

variable "vpc_id" {
  description = "The identifier of the VPC in which to create the target group"
  type        = string
}

variable "health_check_path" {
  description = "The destination for the health check request"
  default     = "/"
  type        = string
}

variable "client_host_header" {
  description = "Path pattern to match against the request URL"
  type        = list(string)
}

variable "listener_arn" {
  description = "The ARN of the listener to which to attach the rule"
  type        = string
}

variable "cluster_arn" {
  description = "ARN of an ECS cluster"
  type        = string
}

variable "cluster_name" {
  description = "Name of an ECS cluster"
  type        = string
}

variable "task_definition" {
  description = "The family and revision (family:revision) or full ARN of the task definition that you want to run in your service"
  type        = string
}

variable "desired_count" {
  description = "The number of instances of the task definition to place and keep running"
  default     = 0
  type        = number
}

variable "max_capacity" {
  description = "Maximum capacity of service"
  default     = 0
  type        = number
}

variable "min_capacity" {
  description = "Minimum capacity of service"
  default     = 0
  type        = number
}

variable "target_value" {
  description = "Target value of service"
  default     = 0
  type        = number
}

variable "scheduling_strategy" {
  description = "The scheduling strategy to use for the service. The valid values are REPLICA and DAEMON"
  default     = "REPLICA"
  type        = string
}

variable "deployment_maximum_percent" {
  description = "The upper limit (as a percentage of the service's desiredCount) of the number of running tasks that can be running in a service during a deployment"
  default     = 200
  type        = number
}

variable "deployment_minimum_healthy_percent" {
  description = "The lower limit (as a percentage of the service's desiredCount) of the number of running tasks that must remain running and healthy in a service during a deployment"
  default     = 100
  type        = number
}

variable "lb_client_certificate_arn" {
  description = "ARN for the load balancer certificate"
  type        = string
}

variable "tags" {
  description = "Tags to be applied to the resource"
  default     = {}
  type        = map
}
