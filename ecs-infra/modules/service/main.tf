locals {
  default_tags = {
    Environment = terraform.workspace
    Name        = "${var.identifier}-${terraform.workspace}"
  }
  tags = merge(local.default_tags, var.tags)
}

resource "aws_ecs_service" "service" {
  name                = "${var.identifier}-${terraform.workspace}"
  cluster             = var.cluster_arn
  launch_type         = "FARGATE"
  task_definition     = var.task_definition
  desired_count       = var.desired_count
  scheduling_strategy = var.scheduling_strategy
  platform_version    = "1.4.0"

  deployment_maximum_percent         = var.deployment_maximum_percent
  deployment_minimum_healthy_percent = var.deployment_minimum_healthy_percent

  load_balancer {
    target_group_arn = aws_alb_target_group.client_target.arn
    container_name   = "${var.identifier}-${terraform.workspace}"
    container_port   = var.container_port
  }

  lifecycle {
    ignore_changes = [
      desired_count
    ]
  }

  network_configuration {
    subnets = var.vpc_subnets
    assign_public_ip = false
    security_groups = var.service_asgs
  }

  tags = local.tags

  depends_on = [aws_lb_listener_rule.client_rule]
}

resource "aws_alb_target_group" "client_target" {
  port                 = var.container_port
  name                 = "${var.identifier}-client-${terraform.workspace}"        
  protocol             = "HTTP"
  vpc_id               = var.vpc_id
  target_type          = "ip"
  deregistration_delay = 60
  

  health_check {
    healthy_threshold   = 10
    interval            = 130
    path                = var.health_check_path
    timeout             = 120
    unhealthy_threshold = 10
  }

  tags = local.tags
}

resource "aws_lb_listener_rule" "client_rule" {
  listener_arn = var.listener_arn

  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.client_target.arn
  }

  condition {
    host_header {
      values = var.client_host_header
    }
  }
}

resource "aws_lb_listener_certificate" "certificates" {

  certificate_arn = var.lb_client_certificate_arn
  listener_arn    = var.listener_arn
}

resource "aws_appautoscaling_target" "ecs_target" {
  max_capacity       = var.max_capacity
  min_capacity       = var.min_capacity
  resource_id        = "service/${var.cluster_name}/${aws_ecs_service.service.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

resource "aws_appautoscaling_policy" "cpu_target_policy" {
  name               = "${aws_ecs_service.service.name}_cpu"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.ecs_target.resource_id
  scalable_dimension = aws_appautoscaling_target.ecs_target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.ecs_target.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }

    target_value       = var.target_value
    scale_in_cooldown  = 360
    scale_out_cooldown = 180
  }
}
