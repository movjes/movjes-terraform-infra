variable "identifier" {
  description = "The name for the cluster"
  type        = string
}

variable "region" {
  description = "region"
  type        = string
}

variable "container_memory_reservation" {
    description = "container memory"
    type        = number
    default     = 4096
}

variable "container_memory" {
    description = "container memory"
    default     = 4096
    type        = number
}

variable "container_cpu" {
    description = "container cpu"
    default     = 1024
    type        = number
}

variable "container_image" {
    description = "docker image"
    default     = "ricardcutzh/movjes-odoo:dev"
    type        = string
}

variable "port_mappings" {
  description = "The port mappings to configure for the container. This is a list of maps. Each map should contain \"containerPort\", \"hostPort\", and \"protocol\", where \"protocol\" is one of \"tcp\" or \"udp\". If using containers in a task with the awsvpc or host network mode, the hostPort can either be left blank or set to the same value as the containerPort"
  default     = []
  type = list(object(
    {
      containerPort = number
      hostPort      = number
      protocol      = string
    }
  ))
}

variable "environment" {
  description = "The environment variables to pass to the container. This is a list of maps"
  default     = []
  type = list(object(
    {
      name  = string
      value = string
    }
  ))
}

variable "container_port" {
  description = "The port that the container service runs on"
  type        = number
}

variable "max_capacity" {
  description = "Maximum capacity of service"
  default     = 0
  type        = number
}

variable "min_capacity" {
  description = "Minimum capacity of service"
  default     = 0
  type        = number
}

variable "target_value" {
  description = "Target value of service"
  default     = 0
  type        = number
}

variable "desired_count" {
  description = "The number of instances of the task definition to place and keep running"
  default     = 0
  type        = number
}

variable "client_host_header" {
  description = "Path pattern to match against the request URL"
  type        = list(string)
}

variable "lb_client_certificate_arn" {
  description = "ARN for the load balancer certificate"
  type        = string
}

variable "https_listener" {
    description = "Load balancer listener arn"
    type        = string
}