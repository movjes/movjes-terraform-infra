terraform {
  required_version = "~> 0.12"
  backend "s3" {
    bucket               = "ricardo-test-bucket-personal"
    region               = "us-east-1"
    key                  = "backend-new.tfstate"
    workspace_key_prefix = "odoo-infra"
    dynamodb_table       = "ricardo-terraform"
  }
}