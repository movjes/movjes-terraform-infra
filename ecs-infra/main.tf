module "task_definition" {
    source                       = "./modules/task_definition"


    container_memory_reservation = var.container_memory_reservation
    log_configuration            = {
                                        logDriver = "awslogs",
                                        options   = {
                                            awslogs-region = var.region
                                            awslogs-group  = "${terraform.workspace}/${var.identifier}"
                                            awslogs-stream-prefix  = "ecs-${terraform.workspace}"
                                            awslogs-create-group = true
                                        }
                                 }
    container_memory             = var.container_memory
    container_image              = var.container_image
    port_mappings                = var.port_mappings
    container_cpu                = var.container_cpu
    environment                  = var.environment
    identifier                   = var.identifier
    efs_volume_id                = data.terraform_remote_state.infrastructure.outputs.output.efs_volume_id
}

# Defining service for each client
# the module will be executed according to each client configuration
module "ecs_service" {
    source                       = "./modules/service"

    service_asgs                 = [tostring(data.terraform_remote_state.infrastructure.outputs.output.service_sg)]
    task_definition              = module.task_definition.output.task_definition.arn
    container_port               = var.container_port
    listener_arn                 = var.https_listener
    client_host_header           = var.client_host_header
    vpc_subnets                  = data.terraform_remote_state.infrastructure.outputs.output.application_subnets
    identifier                   = var.identifier
    cluster_arn                  = data.terraform_remote_state.infrastructure.outputs.output.ecs_cluster_arn
    cluster_name                 = data.terraform_remote_state.infrastructure.outputs.output.ecs_cluster_name
    vpc_id                       = data.terraform_remote_state.infrastructure.outputs.output.vpc_id
    lb_client_certificate_arn    = var.lb_client_certificate_arn
    max_capacity                 = var.max_capacity
    min_capacity                 = var.min_capacity
    target_value                 = var.target_value
    depends_on = [
        module.task_definition,
    ]
}
