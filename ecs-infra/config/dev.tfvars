identifier                   = "movjes"
region                       = "us-east-1"
container_memory_reservation = 4096
container_memory             = 4096
container_cpu                = 1024
container_image              = "ricardcutzh/movjes-odoo:dev"
port_mappings                = [
                                    {
                                        containerPort = 8069
                                        hostPort      = 8069
                                        protocol      = "tcp"
                                    }
                                ]
environment                  =  [
                                    {
                                        name          = "TEST"
                                        value         = "TEST"
                                    }
                                ]
container_port               = 8069
max_capacity                 = 1
min_capacity                 = 1
target_value                 = 60
desired_count                = 1
client_host_header           = ["e-learning.ricardcutzh.com"]
lb_client_certificate_arn    = "arn:aws:acm:us-east-1:648600599342:certificate/0a30da05-a0c1-4fb9-91ec-f8edf3a1e63f"
https_listener               = "arn:aws:elasticloadbalancing:us-east-1:648600599342:listener/app/movjes-default/85762e660652e375/69f71b60f34543bd"