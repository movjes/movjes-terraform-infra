data "terraform_remote_state" "infrastructure" {
    workspace                    = terraform.workspace
    backend                      = "s3"
    config = {
        bucket                   = "ricardo-test-bucket-personal"
        region                   = "us-east-1"
        key                      = "backend.tfstate"
        workspace_key_prefix     = "base_infra"
        dynamodb_table           = "ricardo-terraform"
    }
}