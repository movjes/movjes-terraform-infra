region       = "us-east-1"
identifier   = "movjes"
app_subnet1  = "movjes/vpc/app_subnet1"
app_subnet2  = "movjes/vpc/app_subnet2"
pub_subnet1  = "movjes/vpc/pub_subnet1"
pub_subnet2  = "movjes/vpc/pub_subnet2"
data_subnet1 = "movjes/vpc/data_subnet1"
data_subnet2 = "movjes/vpc/data_subnet2"
vpc_id       = "movjes/vpc/id"
cert         = "movjes/domain/cert"

image_id = "ami-0739f8cdb239fe9ae" # ubuntu 18.04
key_name = "movjes-virginia-admin"
volume_size = 50
instance_type = "t2.micro"
min_size = 0
max_size = 0