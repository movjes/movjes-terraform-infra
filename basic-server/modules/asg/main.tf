locals {
  default_tags = {
    Environment = terraform.workspace
    Name        = "${var.identifier}-${terraform.workspace}-ssh"
  }
  tags = merge(local.default_tags, var.tags)
}

resource "aws_iam_policy" "policy_ssh" {
  name        = "${var.identifier}-${terraform.workspace}-ssh-policy"
  path        = "/"
  description = "ssh box permissions"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "*",
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_role" "role_ssh" {
  name = "${var.identifier}-${terraform.workspace}-ssh-profile"
  path = "/"
  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_policy_attachment" "ssh-attach-permissions" {
  name       = "${var.identifier}-${terraform.workspace}-ssh-permissions"
  roles      = [aws_iam_role.role_ssh.name]
  policy_arn = aws_iam_policy.policy_ssh.arn
}

resource "aws_iam_instance_profile" "ssh_profile" {
  name = "${var.identifier}-${terraform.workspace}-ssh-profile"
  role = aws_iam_role.role_ssh.name
}

resource "aws_launch_template" "launch_template" {
  iam_instance_profile {
    arn = aws_iam_instance_profile.ssh_profile.arn
  }
  
  ebs_optimized               = var.ebs_optimized
  instance_type               = var.instance_type
  user_data                   = var.user_data_base64
  key_name                    = var.key_name
  image_id                    = var.image_id
  name                        = "${var.identifier}-${terraform.workspace}-lt"

  network_interfaces {
    associate_public_ip_address = var.associate_public_ip_address
    security_groups             = var.security_groups
  }

  block_device_mappings {
    device_name = "/dev/sda1"

    ebs {
      volume_size = var.volume_size
    }
  }
}

resource "aws_autoscaling_group" "asg" {
  vpc_zone_identifier  = var.subnets
  desired_capacity     = var.min_size
  name_prefix          = "${var.identifier}-${terraform.workspace}-sg"
  max_size             = var.max_size
  min_size             = var.min_size

  depends_on = [aws_launch_template.launch_template]

  health_check_grace_period = 300

  launch_template {
    id      = aws_launch_template.launch_template.id
    version = aws_launch_template.launch_template.latest_version
  }

  instance_refresh {
    strategy = "Rolling"
    preferences {
      min_healthy_percentage = 100
      instance_warmup        = 300
    }
  }

  dynamic "tag" {
    for_each = local.tags
    content {
      propagate_at_launch = true
      value               = tag.value
      key                 = tag.key
    }
  }
}