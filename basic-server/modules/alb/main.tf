locals {
  default_tags = {
    Environment = terraform.workspace
    Name        = "${var.identifier}-${terraform.workspace}"
  }
  tags = merge(local.default_tags, var.tags)
}

resource "aws_lb" "main" {
  security_groups = var.security_groups
  internal        = var.lb_is_internal #tfsec:ignore:AWS005
  subnets         = var.subnet_ids
  name            = "${var.identifier}-${terraform.workspace}"

  tags = local.tags
}

resource "aws_lb_listener" "http_listener" {
  load_balancer_arn = aws_lb.main.id
  protocol          = "HTTP"
  port              = "80"

  default_action {
    type = "redirect"

    redirect {
      status_code = "HTTP_301"
      protocol    = "HTTPS"
      port        = "443"
    }
  }
}

resource "aws_lb_listener" "https_listener" {
  count = length(var.alb_certificate_arn) > 0 ? 1 : 0

  load_balancer_arn = aws_lb.main.id
  certificate_arn   = var.alb_certificate_arn[0]
  ssl_policy        = "ELBSecurityPolicy-TLS-1-2-2017-01"
  protocol          = "HTTPS"
  port              = 443

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "Not Found"
      status_code  = "404"
    }
  }
}

resource "aws_lb_listener_certificate" "certificates" {
  count = length(var.alb_certificate_arn)

  certificate_arn = var.alb_certificate_arn[count.index]
  listener_arn    = aws_lb_listener.https_listener.0.arn

  depends_on = [aws_lb_listener.https_listener]
}
