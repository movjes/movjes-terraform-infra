variable "identifier" {
  description = "The name for the cluster"
  type        = string
}

variable "subnets" {
  description = "subnets to create mount points for efs"
  type        = list(string)
}

variable "efs-sgs" {
  description = "security group access to efs"
  type        = list(string)
}

variable "tags" {
  description = "Tags to be applied to the resource"
  default     = {}
  type        = map
}