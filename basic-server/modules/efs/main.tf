locals {
  default_tags = {
    Environment = terraform.workspace
    Name        = "${var.identifier}-${terraform.workspace}"
  }
  tags = merge(local.default_tags, var.tags)
}

resource "aws_efs_file_system" "file-system" {
  performance_mode = "generalPurpose"

  creation_token   = "${var.identifier}-${terraform.workspace}-efs"
  tags = local.tags
}

resource "aws_efs_mount_target" "mount-targets" {
  count           = length(var.subnets)
  security_groups = var.efs-sgs
  file_system_id  = aws_efs_file_system.file-system.id 
  subnet_id       = var.subnets[count.index]
}