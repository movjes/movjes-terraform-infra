variable "region" {}

variable "identifier" {}

variable "app_subnet1" {}

variable "app_subnet2" {}

variable "pub_subnet1" {}

variable "pub_subnet2" {}

variable "data_subnet1" {}

variable "data_subnet2" {}

variable "vpc_id" {}

variable "cert" {}

variable "image_id" {
  description = "AMI id to use by the autoscaling group"
  type        = string
}

variable "key_name" {
  description = "ssh key pair to access the instances"
  type        = string
}

variable "volume_size" {
  description = "size of the root volume for the instance"
  type        = number
}

variable "instance_type" {
  description = "size of the ec2 instance"
  type        = string
}

variable "min_size" {
  description = "Minimum number of instances in the ASG"
  default     = 1
  type        = number
}

variable "max_size" {
  description = "Maximun number of instances in the ASG"
  default     = 1
  type        = number
}