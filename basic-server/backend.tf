terraform {
  required_version = "~> 0.12"
  backend "s3" {
    bucket               = "movjes-terraform"
    region               = "us-east-1"
    key                  = "backend.tfstate"
    workspace_key_prefix = "basic-server"
    dynamodb_table       = "movjes-terraform"
  }
}