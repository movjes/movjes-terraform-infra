locals {
  prefix = "/${terraform.workspace}"
}

data "aws_ssm_parameter" "app_subnet1" {
  name = "${local.prefix}/${var.app_subnet1}"
}

data "aws_ssm_parameter" "app_subnet2" {
  name = "${local.prefix}/${var.app_subnet2}"
}

data "aws_ssm_parameter" "pub_subnet1" {
  name = "${local.prefix}/${var.pub_subnet1}"
}

data "aws_ssm_parameter" "pub_subnet2" {
  name = "${local.prefix}/${var.pub_subnet2}"
}

data "aws_ssm_parameter" "data_subnet1" {
  name = "${local.prefix}/${var.data_subnet1}"
}

data "aws_ssm_parameter" "data_subnet2" {
  name = "${local.prefix}/${var.data_subnet2}"
}

data "aws_ssm_parameter" "vpc_id" {
  name = "${local.prefix}/${var.vpc_id}"
}

data "aws_ssm_parameter" "cert" {
  name = "${local.prefix}/${var.cert}"
}
