data "aws_availability_zones" "azs" {}

module "alb_security_group" {
  source              = "./modules/securitygroups"
  identifier          = "${var.identifier}-alb"
  vpc                 = data.aws_ssm_parameter.vpc_id.value
  ingress_rule_list   = [
    {
      source_security_group_id = null
      cidr_blocks              = ["0.0.0.0/0"]
      description              = "All Web Traffic (443)"
      from_port                = 443
      protocol                 = "tcp"
      to_port                  = 443
    },
    {
      source_security_group_id = null
      cidr_blocks              = ["0.0.0.0/0"]
      description              = "All Web Traffic (80)"
      from_port                = 80
      protocol                 = "tcp"
      to_port                  = 80
    }
  ]
  egress_rule_list    = [
    {
      source_security_group_id = null
      cidr_blocks              = ["0.0.0.0/0"],
      description              = "Default egress rule",
      from_port                = 0,
      protocol                 = "all",
      to_port                  = 65535
    }
  ]
}

module "app_security_group" {
  source              = "./modules/securitygroups"
  identifier          = "${var.identifier}-app"
  vpc                 = data.aws_ssm_parameter.vpc_id.value
  ingress_rule_list   = [
    {
      source_security_group_id = module.alb_security_group.output.main_sg
      cidr_blocks              = null
      description              = "All Traffic from alb"
      from_port                = 0
      protocol                 = "all"
      to_port                  = 65535
    },
    {
      source_security_group_id = null
      cidr_blocks              = ["0.0.0.0/0"]
      description              = "SSH"
      from_port                = 22
      protocol                 = "tcp"
      to_port                  = 22
    }
  ]
  egress_rule_list    = [
    {
      source_security_group_id = null
      cidr_blocks              = ["0.0.0.0/0"],
      description              = "Default egress rule",
      from_port                = 0,
      protocol                 = "all",
      to_port                  = 65535
    }
  ]
}

module "efs_security_group" {
  source              = "./modules/securitygroups"
  identifier          = "${var.identifier}-efs"
  vpc                 = data.aws_ssm_parameter.vpc_id.value
  ingress_rule_list   = [
    {
      source_security_group_id = module.app_security_group.output.main_sg
      cidr_blocks              = null
      description              = "All Traffic from app"
      from_port                = 2049
      protocol                 = "tcp"
      to_port                  = 2049
    }
  ]
  egress_rule_list    = [
    {
      source_security_group_id = null
      cidr_blocks              = ["0.0.0.0/0"],
      description              = "Default egress rule",
      from_port                = 0,
      protocol                 = "all",
      to_port                  = 65535
    }
  ]
}

module "alb" {
  source              = "./modules/alb"
  identifier          = var.identifier
  security_groups     = [tostring(module.alb_security_group.output.main_sg)]
  subnet_ids          = [data.aws_ssm_parameter.pub_subnet1.value, data.aws_ssm_parameter.pub_subnet2.value]
  vpc_id              = data.aws_ssm_parameter.vpc_id.value
  alb_certificate_arn = [data.aws_ssm_parameter.cert.value]
}

module "efs" {
  source              = "./modules/efs"
  identifier          = var.identifier
  efs-sgs             = [tostring(module.efs_security_group.output.main_sg)]
  subnets             = [data.aws_ssm_parameter.data_subnet1.value, data.aws_ssm_parameter.data_subnet2.value]
}

# creating the autoscaling group resource for the ssh bastion
data "template_file" "user_data" {
  template = file("./templates/ec2-user-data.sh")
  vars = {
    efs_id = module.efs.output.efs_id
  }
}

module "asg" {
    source = "./modules/asg"
    associate_public_ip_address = true
    user_data_base64     = base64encode(data.template_file.user_data.rendered)
    security_groups      = [tostring(module.app_security_group.output.main_sg)]
    identifier           = var.identifier
    image_id             = var.image_id
    key_name             = var.key_name
    volume_size          = var.volume_size
    instance_type        = var.instance_type
    subnets              = [data.aws_ssm_parameter.app_subnet2.value, data.aws_ssm_parameter.pub_subnet1.value]
    min_size             = var.min_size
    max_size             = var.max_size
    depends_on = [
      module.efs,
    ]
}